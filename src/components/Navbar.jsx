import navIcon from '/favicon.svg'

export default function Navbar() {
  return (
    <nav>
      <img src={navIcon} alt="Site icon" /><span> my travel journal.</span>
    </nav>
  )
} 