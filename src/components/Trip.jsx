export default function Trip(props) {
  console.log()
  return (
    <div className="trip">
      <img className="trip--cover" src={`../src/images/${props.image}`} alt={props.title} />
      <div className="trip--info">
        <div className="trip--location"><img src="../src/assets/location-dot-solid.svg" alt="" /><span className="trip--location-name"> {props.location}</span><a className="trip--location-url" href={props.mapsUrl} target="_blank">View on Google Maps</a></div>
        <h1 className="trip--title">{props.title}</h1>
        <p className="trip--dates">{`${props.dates.startDate} - ${props.dates.endDate}`}</p>
        <p className="trip--description">{props.description}</p>
      </div>
    </div>
  )
}