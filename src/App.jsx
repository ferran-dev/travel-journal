import Navbar from './components/Navbar'
import Trip from './components/Trip'
import tripsData from './data'
import './styles.css'

function App() {

  const trips = tripsData.map(trip => {
    return (
      <Trip
        key={trip.id}
        {...trip}
      />
    )
  }
  )

  return (
    <>
      <Navbar />
      <section className='trip-list'>
        {trips}
      </section>
    </>
  )
}

export default App
