export default [
  {
    id: 1,
    location: "Italy",
    mapsUrl: "https://goo.gl/maps/VV2pRTb5dnCrwoTA9",
    title: "Roman Coliseu",
    dates: {
      startDate: "14 May, 2017",
      endDate: "20 May, 2017",
    },
    description:
      "The Colosseum is an elliptical amphitheatre in the centre of the city of Rome. It is the largest ancient amphitheatre ever built, and is still the largest standing amphitheatre in the world, despite its age. It was used for gladiatorial contests and public spectacles mainly.",
    image: "coliseum.jpg",
  },
  {
    id: 2,
    location: "Ireland",
    mapsUrl: "https://goo.gl/maps/n5E8wNZgqZAUidW36",
    title: "Cliffs of Moher",
    dates: {
      startDate: "30 June, 2021",
      endDate: "31 August, 2021",
    },
    description:
      "The Cliffs of Moher are sea cliffs located at the southwestern edge of the Burren region in County Clare. They run for about 14 kilometres and the highest point is 214 metres. Also, is one of the most touristic attractions in Ireland.",
    image: "cliffs-of-moher.jpg",
  },
  {
    id: 3,
    location: "England",
    mapsUrl: "https://goo.gl/maps/n5E8wNZgqZAUidW36",
    title: "Big Ben",
    dates: {
      startDate: "4 June, 2022",
      endDate: "6 June, 2022",
    },
    description:
      "Big Ben is the nickname for the Great Bell of the Great Clock of Westminster. When completed in 1859, its clock was the largest and most accurate four-faced striking and chiming clock in the world. It is one of the most prominent symbols of the United Kingdom and parliamentary democracy.",
    image: "big-ben.jpg",
  },
];
